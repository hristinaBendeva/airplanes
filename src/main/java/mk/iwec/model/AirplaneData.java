package mk.iwec.model;

public class AirplaneData {
	private int seats;
	private int fuel;

	public AirplaneData() {
		super();
	}

	public AirplaneData(int seats, int fuel) {
		super();
		this.seats = seats;
		this.fuel = fuel;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}



	@Override
	public String toString() {
		return "Data [seats=" + seats + ", fuel=" + fuel + "]";
	}

}
