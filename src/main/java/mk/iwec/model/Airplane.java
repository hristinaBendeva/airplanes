package mk.iwec.model;

public class Airplane {
	private Integer id;
	private String company;
	private String model;
	private int seats;
	private int fuelCapacity;
	private AirplaneData data;

	public Airplane() {
		super();
	}

	public Airplane(Integer id, String company, String model, int seats, int fuelCapacity) {
		super();
		this.id = id;
		this.company = company;
		this.model = model;
		this.seats = seats;
		this.fuelCapacity = fuelCapacity;
	}

	public Airplane(Integer id, String company, String model, int seats, int fuelCapacity, AirplaneData data) {
		super();
		this.id = id;
		this.company = company;
		this.model = model;
		this.seats = seats;
		this.fuelCapacity = fuelCapacity;
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public int getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(int fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public AirplaneData getData() {
		return data;
	}

	public void setData(AirplaneData data) {
		this.data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airplane other = (Airplane) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return id + ", " + company + ", " + model + ", " + seats + ", " + fuelCapacity;
	}

}
