package mk.iwec.file;

import java.util.List;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import mk.iwec.model.Airplane;
import mk.iwec.model.AirplaneData;
import mk.iwec.parser.MyWrapper;

@SpringBootApplication
public class FWManager extends FileManagerImpl<Airplane> {
	private static final String INPUT_CSV = "input/planes.csv";
	FileManager<Airplane> reader = new CSVManager();

	@Override
	public Airplane createInstance(String line) {
		Airplane airplane = new Airplane();
		if (line == null || line.isEmpty() || line.length() != 8) {
			return null;
		}
		List<Airplane> airplanes = reader.read(INPUT_CSV);
		for (Airplane it : airplanes) {
			AirplaneData data = new AirplaneData();
			Integer id = MyWrapper.parseInt(line.substring(0, 2));
			Integer seats = MyWrapper.parseInt(line.substring(2, 5));
			Integer fuel = MyWrapper.parseInt(line.substring(5, 8));
			if (id == 0 || id == null || seats == null || fuel == null) {
				return null;
			}
			data.setSeats(seats);
			data.setFuel(fuel);
			if (it.getId() == id) {
				airplane.setId(it.getId());
				airplane.setCompany(it.getCompany());
				airplane.setModel(it.getModel());
				airplane.setSeats(it.getSeats());
				airplane.setFuelCapacity(it.getFuelCapacity());
				airplane.setData(data);
			}
		}
		return airplane;
	}

}
