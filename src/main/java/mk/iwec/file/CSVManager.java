package mk.iwec.file;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import mk.iwec.model.Airplane;
import mk.iwec.parser.MyWrapper;

@SpringBootApplication
public class CSVManager extends FileManagerImpl<Airplane> {

	@Override
	public Airplane createInstance(String line) {
		if (line == null || line.isEmpty()) {
			return null;
		}
		String[] tokens = line.split(",");
		if (tokens.length != 5) {
			return null;
		}
		Integer id = MyWrapper.parseInt(tokens[0].trim());
		int seats = MyWrapper.parseInt(tokens[3].trim());
		int fuel = MyWrapper.parseInt(tokens[4].trim());

		return new Airplane(id, tokens[1].trim(), tokens[2].trim(), seats, fuel);
	}

}
