package mk.iwec.file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public abstract class FileManagerImpl<T> implements FileManager<T> {

	@Override
	public List<T> read(String fileName) {
		List<T> result = new LinkedList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
			String line;
			while ((line = reader.readLine()) != null) {
				T t = createInstance(line);
				if (t != null) {
					result.add(t);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public boolean write(List<T> content, String fileName) {
		boolean ok = true;
		try (PrintWriter write = new PrintWriter(fileName)) {
			for (T t : content) {
				write.println(t);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return ok;
	}

	public abstract T createInstance(String line);
}
