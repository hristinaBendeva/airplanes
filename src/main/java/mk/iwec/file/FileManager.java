package mk.iwec.file;

import java.util.List;

public interface FileManager<T> {
	public List<T> read(String fileName);

	public boolean write(List<T> content, String fileName);
}
