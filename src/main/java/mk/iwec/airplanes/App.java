package mk.iwec.airplanes;

import java.util.List;

import org.springframework.stereotype.Component;

import mk.iwec.file.FWManager;
import mk.iwec.file.FileManager;
import mk.iwec.model.Airplane;
import mk.iwec.service.FilterPlanes;

@Component
public class App {
	private static final String INPUT_FW_FILE = "input/data.txt";
	private static final String OUTPUT_FILE = "output/planes.csv";

	public void run() {
		FileManager<Airplane> read = new FWManager();
		List<Airplane> airplanes = read.read(INPUT_FW_FILE);
		airplanes.stream().forEach(System.out::println);
		FilterPlanes filter = new FilterPlanes();
		List<Airplane> result = filter.readyToFly(airplanes);
		result.stream().forEach(System.out::println);
		System.out.println(read.write(result, OUTPUT_FILE));
	}

}
