package mk.iwec.airplanes;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirplanesApplication {
	@Autowired
	App app;

	public static void main(String[] args) {
		SpringApplication.run(AirplanesApplication.class, args);

	}

	@PostConstruct
	public void start() {
		app.run();
	}
}
