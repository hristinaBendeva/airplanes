package mk.iwec.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import mk.iwec.model.Airplane;

@SpringBootApplication
public class FilterPlanes {

	public List<Airplane> readyToFly(List<Airplane> airplanes) {
		if (airplanes == null || airplanes.isEmpty()) {
			return null;
		}
		List<Airplane> result = new LinkedList<>();
		for (Airplane airplane : airplanes) {
					float s = airplane.getSeats() - (airplane.getSeats() * (20.0f / 100.0f));
					float f = (airplane.getFuelCapacity() * (70.0f / 100.0f));
					if (s <= airplane.getData().getSeats() || f <= airplane.getData().getFuel()) {
						result.add(airplane);
					}
		}
		return result;
	}
}
