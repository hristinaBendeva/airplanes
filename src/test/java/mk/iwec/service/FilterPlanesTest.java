package mk.iwec.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import mk.iwec.model.Airplane;
import mk.iwec.model.AirplaneData;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FilterPlanes.class)
public class FilterPlanesTest {
	@Spy
	List<Airplane> airplanes = new LinkedList<>();

	@InjectMocks
	private FilterPlanes filter;

	@Before
	public void setUp() {
		filter = new FilterPlanes();
	}

	@Test
	public void testWhenListsHaveElements() {
		airplanes.add(new Airplane(1, "WizzAir", "Ilyushin Il-86", 20, 50, new AirplaneData(15, 25)));
		airplanes.add(new Airplane(2, "Turkish Airlines", "Beechcraft M420", 70, 100, new AirplaneData(60, 70)));
		Mockito.verify(airplanes).add(new Airplane(1, "WizzAir", "Ilyushin Il-86", 20, 50, new AirplaneData(15, 25)));
		Mockito.verify(airplanes).add(new Airplane(2, "Turkish Airlines", "Beechcraft M420", 70, 100, new AirplaneData(60, 70)));

		List<Airplane> result = filter.readyToFly(airplanes);
		List<Airplane> list = new LinkedList<>();
		list.add(new Airplane(2, "Turkish Airlines", "Beechcraft M420", 70, 100));

		assertEquals(list, result);
	}

	@Test
	public void testWhenListsAreNull() {
		List<Airplane> airplanes = filter.readyToFly(null);

		assertEquals(null, airplanes);
	}

	@Test
	public void testWhenListsAreEmplty() {
		when(airplanes.isEmpty()).thenReturn(true);

		List<Airplane> result = filter.readyToFly(airplanes);

		assertEquals(null, result);
	}
}
